module.exports = function(app, prisma) {
    const Role = prisma._dmmf.enumMap.Role.values.reduce(function(map, obj) { map[obj] = obj; return map; }, {});

    app.get('/arsip', async function(req, res) {
        try {
            var { userId, idTim } = req.query;
            if (typeof userId === 'undefined' ||
                typeof idTim === 'undefined') {
                    res.status(400).send({
                        "error": "All input is required",
                    });
                    return;
            }
            idTim = Number(idTim);
            if (typeof userId !== 'string' ||
                typeof idTim !== 'number') {
                    res.status(400).send({
                        "error": "Bad input format",
                    });
                    return;
            }

            // mengecek apakah tim tersebut ada
            var tim = await prisma.tim.findUnique({
                where: {
                    id: idTim,
                },
            });
            if (!tim) {
                res.status(404).send({
                    "error": "Tim not found",
                });
                return;
            }

            // mengecek apakah user berada di dalam tim tersebut dengan role ADMINISTRATOR
            var roleDalamTim = await prisma.roleDalamTim.findUnique({ 
                where: {
                    userId_idTim: {userId, idTim},
                },
            });

            if (!roleDalamTim || !(roleDalamTim.role == Role.ADMINISTRATOR)) {
                res.status(403).send({
                    "error": "No access",
                });
                return;
            }

            // mengembalikan semua permintaan publikasi di tim tersebut
            var permintaanPublikasi = await prisma.permintaanPublikasi.findMany({
                where: {
                    idTim: idTim,
                }
            });
            res.status(200).send({
                permintaanPublikasi,
            });
        } catch (err) {
            console.log(err);
        }
    });
}
