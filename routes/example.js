module.exports = function(app) {

    app.get('/test/:id', function(req, res) {
        res.send({'param': req.params.id});
    });

    app.get('/', function(req, res) {
        res.send('Root directory');
    });
};
