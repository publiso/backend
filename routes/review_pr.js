module.exports = function(app, prisma) {
    const Status = prisma._dmmf.enumMap.Status.values.reduce(function(map, obj) { map[obj] = obj; return map; }, {});;
    const Role = prisma._dmmf.enumMap.Role.values.reduce(function(map, obj) { map[obj] = obj; return map; }, {});

    app.post('/review_pr', async function(req, res) {
        try {
            var {userId, idPermintaanPublikasi, terima} = req.body;
            
            if (typeof userId === 'undefined' ||
                typeof idPermintaanPublikasi === 'undefined' ||
                typeof terima === 'undefined') {
                    res.status(400).send({
                        "error": "All input is required",
                    });
                    return;
            }
            idPermintaanPublikasi = Number(idPermintaanPublikasi);
            if (typeof userId !== 'string' ||
                typeof idPermintaanPublikasi !== 'number' ||
                typeof terima !== 'boolean') {
                    res.status(400).send({
                        "error": "Bad input format",
                    });
                    return;
            }

            // validasi apakah permintaan publikasi ada dengan status dalam_review_pr
            var permintaanPublikasi = await prisma.permintaanPublikasi.findUnique({ 
                where: {
                    id: idPermintaanPublikasi,
                },
            });

            if (!permintaanPublikasi ||
                !(permintaanPublikasi.status == Status.dalam_review_pr)) {
                        res.status(404).send({
                            "error": "PermintaanPublikasi not found",
                        });
                        return;
                    }

            // validasi apakah user ada dalam tim dan memiliki role PR atau ADMINISTRATOR
            var idTim = permintaanPublikasi.idTim;
            var roleDalamTim = await prisma.roleDalamTim.findUnique({ 
                where: {
                    userId_idTim: {userId, idTim},
                },
            });

            if (!roleDalamTim || !(roleDalamTim.role == Role.PR || roleDalamTim.role == Role.ADMINISTRATOR)) {
                res.status(403).send({
                    "error": "No access",
                });
                return;
            }

            // update status
            permintaanPublikasi = await prisma.permintaanPublikasi.update ({
                where: {
                    id: idPermintaanPublikasi,
                },
                data: {
                    status: (terima ? Status.dalam_review_dsg : Status.ditolak),  
                },
            });

            res.status(204).send();
        } catch (err) {
            console.log(err);
        }
    });
}