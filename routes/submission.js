const { prisma, isUser } = require('../prisma');

// https://stackoverflow.com/a/15855457
const RE = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i;

function validateUrl(value) {
    return RE.test(value);
}

module.exports = function(app) {
    
    app.post('/submit/:id', async function (req, res) {
        const { userId } = req.cookies;
        const loggedIn = await isUser(userId); 
        if (!loggedIn)
            return res.statusCode(401).send('Unauthorized');

        const paramId = req?.params?.id;
        if (paramId === null || paramId === undefined)
            return res.status(403).send('path param `id` is not sufficent');

        const { designUri } = req.body;
        const uri = new URL(designUri);
        const isValid = validateUrl(uri);
        if (!isValid)
            return res.status(403).send('Invalid Uri');        
        let result = undefined;
        try {
            result =  await prisma.permintaanPublikasi.update({
                where: {
                    id: paramId
                },
                data: {
                    hasilDesain: uri.toString(),
                    status: "dalam_verifikasi",
                }
            
            });
        } catch (e) {
            if (e instanceof prisma.PrismaClientKnownRequestError) {
                res.statusCode(500).send('Encountered problem when updating value, please make' +
                                        ' sure you entered the correct data');
            }
        }

        if (result === null || result === undefined)
            return res.status(404).send('Not Found');
        else
            res.send('Updated');
    });
};