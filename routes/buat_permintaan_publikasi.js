module.exports = function(app, prisma) {
    const Status = prisma._dmmf.enumMap.Status.values.reduce(function(map, obj) { map[obj] = obj; return map; }, {});
    const Role = prisma._dmmf.enumMap.Role.values.reduce(function(map, obj) { map[obj] = obj; return map; }, {});

    app.post('/buat_permintaan_publikasi', async function(req, res) {
        try {
            var { userId, idTim, judulPublikasi, idLinePemesan, tanggalWaktuPublikasi, bahanKonten, isMendesak, buktiMendesak, mediaSosial, catatan } = req.body;
            
            if (typeof userId === 'undefined' ||
                typeof idTim === 'undefined' ||
                typeof judulPublikasi  === 'undefined' ||
                typeof idLinePemesan === 'undefined' ||
                typeof tanggalWaktuPublikasi === 'undefined' ||
                typeof bahanKonten === 'undefined' ||
                typeof isMendesak === 'undefined' ||
                typeof mediaSosial === 'undefined' ||
                typeof catatan === 'undefined' ||
                (isMendesak === true && !buktiMendesak)) {
                    res.status(400).send({
                        "error": "All input is required",
                    });
                    return;
            }

            validatedTanggalWaktuPublikasi = (new Date(tanggalWaktuPublikasi)).toString();
            idTim = Number(idTim);
            if (typeof userId !== 'string' ||
                typeof idTim !== 'number' ||
                typeof idLinePemesan !== 'string' ||
                typeof judulPublikasi !== 'string' ||
                validatedTanggalWaktuPublikasi === 'Invalid Date' ||
                typeof bahanKonten !== 'string' ||
                typeof isMendesak !== 'boolean' ||
                typeof mediaSosial !== 'object' ||
                typeof catatan !== 'string' ||
                (isMendesak === true && (typeof buktiMendesak !== 'string'))) {
                    res.status(400).send({
                        "error": "Bad input format",
                    });
                    return;
            }

            // mengecek apakah tim tersebut ada
            var tim = await prisma.tim.findUnique({
                where: {
                    id: idTim,
                },
            });
            if (!tim) {
                res.status(404).send({
                    "error": "Tim not found",
                });
                return;
            }

            // mengecek apakah user berada di dalam tim tersebut dengan role GA atau ADMINISTRATOR
            var roleDalamTim = await prisma.roleDalamTim.findUnique({ 
                where: {
                    userId_idTim: {userId, idTim},
                },
            });

            if (!roleDalamTim && !(roleDalamTim.role == Role.GA || roleDalamTim.role == Role.ADMINISTRATOR)) {
                res.status(403).send({
                    "error": "No access",
                });
                return;
            }

            // mengecek apakah seluruh media sosial milik tim
            var mediaSosialMilikTim = await prisma.mediaSosial.findMany({
                where: {
                    idTim: idTim,
                },
                select: {
                    id: true,
                },
            });
            
            for (let i = 0; i < mediaSosial.length; i++) {
                var idMediaSosial = mediaSosial[i].id;
                var yes = false;
                for (let j = 0; j < mediaSosialMilikTim.length; j++) {
                    if (idMediaSosial === mediaSosialMilikTim[i].id) {
                        yes = true;
                        break;
                    }
                }
                
                if (!yes) {
                    res.status(404).send({
                        "error": "MediaSosial not found",
                    });
                    return;
                }
            }

            // memasukkan ke database
            var permintaanPublikasi = await prisma.permintaanPublikasi.create({
                data: {
                    userId: userId,
                    idTim: idTim,
                    status: Status.dalam_review_pr,
                    idLinePemesan: idLinePemesan,
                    tanggalWaktuPublikasi: tanggalWaktuPublikasi,
                    judulPublikasi: judulPublikasi,
                    bahanKonten: bahanKonten,
                    buktiMendesak: (isMendesak ? buktiMendesak : null),
                    isMendesak: isMendesak,
                    catatan: catatan,
                },
            });


            for (let i = 0; i < mediaSosial.length; i++) {
                var idMediaSosial = mediaSosial[i].id;
                await prisma.mediaSosialUntukPermintaanPublikasi.create({
                    data: {
                        idPermintaanPublikasi: permintaanPublikasi.id,
                        idMediaSosial: idMediaSosial,
                    },
                });
            }

            permintaanPublikasi = await prisma.permintaanPublikasi.findUnique({
                where: {
                    id: permintaanPublikasi.id,
                },
                include: {
                    MediaSosialUntukPermintaanPublikasi: {
                        select: {
                            id: false,
                            idPermintaanPublikasi: false,
                            MediaSosial: {
                                select: {
                                    namaMediaSosial: true,
                                    username: true,
                                },
                            },
                        },
                    },
                }
            });

            res.status(201).send({
                permintaanPublikasi,
            });
        } catch (err) {
            console.log(err);
        }
    });
}
