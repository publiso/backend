
module.exports = function(app, prisma) {
    const Role = prisma._dmmf.enumMap.Role.values.reduce(function(map, obj) { map[obj] = obj; return map; }, {});
    
    app.get('/role_dalam_tim', async function(req, res) {
        try {
            var { userId, idTim } = req.query;

            if (typeof userId === 'undefined') {
                res.status(400).send({
                    "error": "userId is required",
                });
                return;
            }
            if (typeof userId !== 'string') {
                res.status(400).send({
                    "error": "Bad input format",
                });
                return;
            }

            idTim = Number(idTim);
            if (typeof idTim === 'number') {
                // mengembalikan tim yang memiliki idTim tersebut beserta role yang dimiliki user
                var roleDalamTim = await prisma.roleDalamTim.findMany({
                    where: {
                        userId: userId,
                        idTim: idTim,
                    },
                    select: {
                        role: true,
                    }
                });
    
                res.status(200).send({
                    roleDalamTim,
                });
                return;
            }

            // mengembalikan semua tim dengan rolenya yang dimiliki user
            var roleDalamTim = await prisma.roleDalamTim.findMany({
                where: {
                    userId: userId,
                },
                select: {
                    role: true,
                    idTim: true,
                }
            });

            res.status(200).send({
                roleDalamTim,
            });
        } catch (err) {
            console.log(err);
        }
    });
}