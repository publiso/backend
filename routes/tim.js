module.exports = function(app, prisma) {
    const Role = prisma._dmmf.enumMap.Role.values.reduce(function(map, obj) { map[obj] = obj; return map; }, {});

    app.get('/tim', async function(req, res) {
        try {
            var { userId, idTim } = req.query;
            
            if (typeof userId === 'undefined' ||
                typeof idTim === 'undefined') {
                    res.status(400).send({
                        "error": "All input is required",
                    });
                    return;
            }
            idTim = Number(idTim);
            if (typeof userId !== 'string' ||
                typeof idTim !== 'number') {
                    res.status(400).send({
                        "error": "Bad input format",
                    });
                    return;
            }

            // mengecek apakah tim tersebut ada
            var tim = await prisma.tim.findUnique({
                where: {
                    id: idTim,
                },
            });
            if (!tim) {
                res.status(404).send({
                    "error": "Tim not found",
                });
                return;
            }

            // mengecek apakah user berada di dalam tim tersebut
            var roleDalamTim = await prisma.roleDalamTim.findUnique({ 
                where: {
                    userId_idTim: {userId, idTim},
                },
            });

            if (!roleDalamTim) {
                res.status(403).send({
                    "error": "No access",
                });
                return;
            }

            res.status(200).send({
                tim,
            });
        } catch (err) {
            console.log(err);
        }
    });

    app.post('/tim', async function(req, res) {
        try {
            var { userId, namaTim } = req.body;
            
            if (typeof userId === 'undefined' ||
                typeof namaTim === 'undefined') {
                    res.status(400).send({
                        "error": "All input is required",
                    });
                    return;
            }
            if (typeof userId !== 'string' || 
                typeof namaTim !== 'string') {
                    res.status(400).send({
                        "error": "Bad input format",
                    });
                    return;
            }

            // membuat tim baru
            var tim = await prisma.tim.create({
                data: {
                    namaTim: namaTim,
                },
            });

            // menambahkan pembuat tim sebagai administrator
            var roleDalamTim = await prisma.roleDalamTim.create({
                data: {
                    userId: userId,
                    role: Role.ADMINISTRATOR,
                    idTim: tim.id,
                }
            });
            
            res.status(201).send({
                tim,
            });
        } catch (err) {
            console.log(err);
        }
    });

    app.put('/tim', async function(req, res) {
        try {
            var { userId, idTim, namaTim } = req.body;
            
            if (typeof userId === 'undefined' ||
                typeof idTim === 'undefined' ||
                typeof namaTim === 'undefined') {
                    res.status(400).send({
                        "error": "All input is required",
                    });
                    return;
            }
            idTim = Number(idTim);
            if (typeof userId !== 'string' || 
                typeof idTim !== 'number' ||
                typeof namaTim !== 'string') {
                    res.status(400).send({
                        "error": "Bad input format",
                    });
                    return;
            }

            // mengecek apakah tim tersebut ada
            var tim = await prisma.tim.findUnique({
                where: {
                    id: idTim,
                },
            });
            if (!tim) {
                res.status(404).send({
                    "error": "Tim not found",
                });
                return;
            }

            // mengecek apakah pengguna adalah ADMINISTRATOR di dalam tim tersebut
            var roleDalamTim = await prisma.roleDalamTim.findUnique({ 
                where: {
                    userId_idTim: {userId, idTim},
                },
            });

            if (!roleDalamTim || !(roleDalamTim.role == Role.ADMINISTRATOR)) {
                res.status(403).send({
                    "error": "No access",
                });
                return;
            }

            // mengubah nama tim
            var tim = await prisma.tim.update ({
                where: {
                    id: idTim,
                },
                data: {
                    namaTim: namaTim,
                },
            });

            res.status(200).send({
                tim,
            });
        } catch (err) {
            console.log(err);
        }
    });
}