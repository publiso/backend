module.exports = function(app, prisma) {
    const Role = prisma._dmmf.enumMap.Role.values.reduce(function(map, obj) { map[obj] = obj; return map; }, {});
    const Status = prisma._dmmf.enumMap.Status.values.reduce(function(map, obj) { map[obj] = obj; return map; }, {});

    app.get('/get_permintaan_publikasi/by_status', async function(req, res) {
        try {
            var {userId, idTim, status} = req.query;

            if (typeof userId === 'undefined' || 
                typeof idTim === 'undefined' || 
                typeof status === 'undefined') {
                    res.status(400).send({
                        "error": "All input is required",
                    });
                    return;
            }
            idTim = Number(idTim);

            if (typeof userId !== 'string' ||
                typeof idTim !== 'number' ||
                typeof status !== 'string') {
                    res.status(400).send({
                        "error": "Bad input format",
                    });
                    return;
            }

            // mengecek apakah tim tersebut ada
            var tim = await prisma.tim.findUnique({
                where: {
                    id: idTim,
                },
            });
            if (!tim) {
                res.status(404).send({
                    "error": "Tim not found",
                });
                return;
            }

            // mengecek apakah status valid
            if (!(status in Status)) {
                res.status(404).send({
                    "error": "Status not found",
                });
                return;
            }

            // validasi apakah user ada dalam tim
            var roleDalamTim = await prisma.roleDalamTim.findUnique({ 
                where: {
                    userId_idTim: {userId, idTim},
                },
            });

            if (!roleDalamTim) {
                res.status(403).send({
                    "error": "No access",
                });
                return;
            }

            // cari permintaan publikasi ada di dalam tim dengan status yang diminta
            // jika pengguna merupakan GA, maka hanya tampilkan yang menjadi miliknya
            if (roleDalamTim.role == Role.GA) {
                var permintaanPublikasi = await prisma.permintaanPublikasi.findMany({ 
                    where: {
                        userId: userId,
                        idTim: idTim,
                        status: status,
                    },
                    include: {
                        MediaSosialUntukPermintaanPublikasi: {
                            select: {
                                id: false,
                                idPermintaanPublikasi: false,
                                MediaSosial: {
                                    select: {
                                        namaMediaSosial: true,
                                        username: true,
                                    },
                                },
                            },
                        },
                    },
                });

                res.status(200).send({
                    permintaanPublikasi,
                });
                return;
            }

            var permintaanPublikasi = await prisma.permintaanPublikasi.findMany({ 
                where: {
                    idTim: idTim,
                    status: status,
                },
                include: {
                    MediaSosialUntukPermintaanPublikasi: {
                        select: {
                            id: false,
                            idPermintaanPublikasi: false,
                            MediaSosial: {
                                select: {
                                    namaMediaSosial: true,
                                    username: true,
                                },
                            },
                        },
                    },
                },
            });
            res.status(200).send({
                permintaanPublikasi,
            });
        } catch(err) {
            console.log(err);
        }
    });

    app.get('/get_permintaan_publikasi/by_id', async function(req, res) {
        try {
            var {userId, idPermintaanPublikasi} = req.query;

            if (typeof userId === 'undefined' ||
                typeof idPermintaanPublikasi === 'undefined') {
                    res.status(400).send({
                        "error": "All input is required",
                    });
                    return;
            }
            idPermintaanPublikasi = Number(idPermintaanPublikasi);
            if (typeof userId !== 'string' ||
                typeof idPermintaanPublikasi !== 'number') {
                    res.status(400).send({
                        "error": "Bad input format",
                    });
                    return;
            }

            // validasi apakah permintaan publikasi ada
            var permintaanPublikasi = await prisma.permintaanPublikasi.findUnique({ 
                where: {
                    id: idPermintaanPublikasi,
                },
                include: {
                    MediaSosialUntukPermintaanPublikasi: {
                        select: {
                            id: false,
                            idPermintaanPublikasi: false,
                            MediaSosial: {
                                select: {
                                    namaMediaSosial: true,
                                    username: true,
                                },
                            },
                        },
                    },
                },
            });

            if (!permintaanPublikasi) {
                res.status(404).send({
                    "error": "PermintaanPublikasi not found",
                });
                return;
            }

            // validasi apakah user ada dalam tim
            var idTim = permintaanPublikasi.idTim;
            var roleDalamTim = await prisma.roleDalamTim.findUnique({ 
                where: {
                    userId_idTim: {userId, idTim},
                },
            });

            if (!roleDalamTim) {
                res.status(403).send({
                    "error": "No access",
                });
                return;
            }

            // mengembalikan permintaan publikasi
            // jika pengguna merupakan GA, maka kembalikan jika itu miliknya
            if (roleDalamTim.role == Role.GA) {
                if (permintaanPublikasi.userId == userId) {
                    res.status(200).send({
                        permintaanPublikasi,
                    });
                    return;
                }
                res.status(403).send({
                    "error": "No access",
                });
                return;
            }

            res.status(200).send({
                permintaanPublikasi,
            });
        } catch(err) {
            console.log(err);
        }
    });

    app.get('/get_permintaan_publikasi/mendesak', async function(req, res) {
        try {
            var { userId, idTim } = req.query;
            
            if (typeof userId === 'undefined' ||
                typeof idTim === 'undefined') {
                    res.status(400).send({
                        "error": "All input is required",
                    });
                    return;
            }

            idTim = Number(idTim);
            if (typeof userId !== 'string' ||
                typeof idTim !== 'number') {
                    res.status(400).send({
                        "error": "Bad input format",
                    });
                    return;
            }

            // mengecek apakah tim tersebut ada
            var tim = await prisma.tim.findUnique({
                where: {
                    id: idTim,
                },
            });
            if (!tim) {
                res.status(404).send({
                    "error": "Tim not found",
                });
                return;
            }

            // mengecek apakah user berada di dalam tim tersebut
            var roleDalamTim = await prisma.roleDalamTim.findUnique({ 
                where: {
                    userId_idTim: {userId, idTim},
                },
            });

            if (!roleDalamTim) {
                res.status(403).send({
                    "error": "No access",
                });
                return;
            }

            // mengembalikan publikasi mendesak di tim tersebut, yang belum dijadwalkan
            // jika pengguna adalah GA, maka yang dikembalikan hanyalah miliknya
            if (roleDalamTim.role == Role.GA) {
                var permintaanPublikasi = await prisma.permintaanPublikasi.findMany({
                    where: {
                        userId: userId,
                        idTim: idTim,
                        isMendesak: true,
                        OR: [
                            {
                                status: Status.dalam_review_pr,
                            },
                            {
                                status: Status.dalam_review_dsg,
                            },
                            {
                                status: Status.menunggu_hasil_desain,
                            },
                            {
                                status: Status.dalam_verifikasi,
                            }
                        ],
                    }
                });
                res.status(200).send({
                    permintaanPublikasi,
                });
                return;    
            }

            var permintaanPublikasi = await prisma.permintaanPublikasi.findMany({
                where: {
                    idTim: idTim,
                    isMendesak: true,
                    OR: [
                        {
                            status: Status.dalam_review_pr,
                        },
                        {
                            status: Status.dalam_review_dsg,
                        },
                        {
                            status: Status.menunggu_hasil_desain,
                        },
                        {
                            status: Status.dalam_verifikasi,
                        }
                    ],
                }
            });
            res.status(200).send({
                permintaanPublikasi,
            });
        } catch (err) {
            console.log(err);
        }
    });
}
