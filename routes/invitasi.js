let e_jwt = require('express-jwt')
let jwt = require('jsonwebtoken')

module.exports = (app, prisma) => {
	// uid disini adalah uid user yang ingin masuk tim! jika sudah masuk tim tsb, maka tidak bisa join lagi!
	app.get('/join/:uid/:t',
		e_jwt({
			secret: process.env.TOKEN_KEY,
			algorithms: ['HS256'],
			requestProperty: 'token',
			getToken(req) {
				if (req.params && req.params.t)
					return req.params.t
				return null
			}
		}),
		async (req, res) => {
			try {
				const anggota = await prisma.roleDalamTim.findUnique({
					where: {
						userId_idTim: { userId: req.params.uid, idTim: req.token.idTim }
					}
				})
				
				if (!anggota) {
					await prisma.roleDalamTim.create({
						data: {
							userId: req.params.uid,
							role: req.token.role,
							idTim: req.token.idTim
						}
					})

					res
						.status(200)
						.json({
							success: true
						})
				} else {
					res
						.status(400)
						.json({
							success: false,
							message: 'already joined the group'
						})
				}
			} catch (err) {
				console.log(err)
				res
					.status(400)
					.json({
						success: false,
						message: 'error'
					})
			}
		},
		async (err, req, res, next) => {
			if (err.name == 'UnauthorizedError') {
				return res.redirect('../')
			}
		}
	)

	app.post('/join/:uid', async (req, res) => {
		let { idTim, role } = req.body;
		if (!req.params || !req.params.uid || !idTim || !role) res.status(400).json({ success: false, message: "All input required"})

		// uid buat autentikasi user! masukan uid seorang ADMINISTRATOR dalam tim dengan id idTim.
		let roleDalamTim = await prisma.roleDalamTim.findUnique({ 
		    where: {
		        userId_idTim: {
				userId: req.params.uid,
				idTim
			}
		    },
		});

		if (!roleDalamTim || roleDalamTim.role !== "ADMINISTRATOR") {
			return res.status(403).send({
				success: false,
				message: 'no access'
			});
		}

		let token = jwt.sign({
			idTim,
			role
		},
		process.env.TOKEN_KEY,
		{
			expiresIn: '1d'
		})

		let invitation_link = `http://${req.headers.host}/join/${token}`
		res
			.status(200)
			.json({
				link: invitation_link
			})
	})
}
