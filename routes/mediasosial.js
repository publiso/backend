module.exports = function(app, prisma) {
    const Role = prisma._dmmf.enumMap.Role.values.reduce(function(map, obj) { map[obj] = obj; return map; }, {});

    app.get('/mediasosial', async function(req, res) {
        try {
            var { userId, idTim } = req.query;
            
            if (typeof userId === 'undefined' ||
                typeof idTim === 'undefined') {
                    res.status(400).send({
                        "error": "All input is required",
                    });
                    return;
            }
            idTim = Number(idTim);
            if (typeof userId !== 'string' ||
                typeof idTim !== 'number') {
                    res.status(400).send({
                        "error": "Bad input format",
                    });
                    return;
            }

            // mengecek apakah tim tersebut ada
            var tim = await prisma.tim.findUnique({
                where: {
                    id: idTim,
                },
            });
            if (!tim) {
                res.status(404).send({
                    "error": "Tim not found",
                });
                return;
            }

            // mengecek apakah user berada di dalam tim tersebut
            var roleDalamTim = await prisma.roleDalamTim.findUnique({ 
                where: {
                    userId_idTim: {userId, idTim},
                },
            });

            if (!roleDalamTim) {
                res.status(403).send({
                    "error": "No access",
                });
                return;
            }

            // mengembalikan semua media sosial di dalam tim tersebut
            // jika user adalah GA atau DSG, maka hanya kembalikan namaMediaSosial
            if (roleDalamTim.role == Role.GA || roleDalamTim.role == Role.DSG) {
                var mediaSosial = await prisma.mediaSosial.findMany({
                    where: {
                        idTim: idTim,
                    },
                    select: {
                        namaMediaSosial: true,
                        username: true,
                    }
                });
    
                res.status(200).send({
                    mediaSosial,
                });
                return;
            }
            
            var mediaSosial = await prisma.mediaSosial.findMany({
                where: {
                    idTim: idTim,
                },
            });

            res.status(200).send({
                mediaSosial,
            });
        } catch (err) {
            console.log(err);
        }
    });

    app.post('/mediasosial', async function(req, res) {
        try {
            var { userId, idTim, namaMediaSosial, username, credential } = req.body;
            
            if (typeof userId === 'undefined' ||
                typeof idTim === 'undefined' ||
                typeof namaMediaSosial === 'undefined' ||
                typeof username === 'undefined' ||
                typeof credential === 'undefined') {
                    res.status(400).send({
                        "error": "All input is required",
                    });
                    return;
            }
            idTim = Number(idTim);
            if (typeof userId !== 'string' ||
                typeof idTim !== 'number' ||
                typeof namaMediaSosial !== 'string' ||
                typeof username !== 'string' ||
                typeof credential !== 'object') {
                    res.status(400).send({
                        "error": "Bad input format",
                    });
                    return;
            }

            // mengecek apakah tim tersebut ada
            var tim = await prisma.tim.findUnique({
                where: {
                    id: idTim,
                },
            });
            if (!tim) {
                res.status(404).send({
                    "error": "Tim not found",
                });
                return;
            }

            // mengecek apakah user berada di dalam tim tersebut dan memiliki role PR atau ADMINISTRATOR
            var roleDalamTim = await prisma.roleDalamTim.findUnique({ 
                where: {
                    userId_idTim: {userId, idTim},
                },
            });

            if (!roleDalamTim || !(roleDalamTim.role == Role.ADMINISTRATOR || roleDalamTim.role == Role.PR)) {
                res.status(403).send({
                    "error": "No access",
                });
                return;
            }

            var mediaSosial = await prisma.mediaSosial.create({
                data: {
                    idTim: idTim,
                    namaMediaSosial: namaMediaSosial,
                    username: username,
                    credential: credential,
                },
            });

            res.status(201).send({
                mediaSosial,                
            });
        } catch (err) {
            console.log(err);
        }
    });

    app.put('/mediasosial', async function(req, res) {
        try {
            var { userId, idMediaSosial, namaMediaSosial, username, credential } = req.body;
            
            if (typeof userId === 'undefined' ||
                typeof idMediaSosial === 'undefined' ||
                typeof namaMediaSosial === 'undefined' ||
                typeof username === 'undefined' ||
                typeof credential === 'undefined') {
                    res.status(400).send({
                        "error": "All input is required",
                    });
                    return;
            }
            idMediaSosial = Number(idMediaSosial);
            if (typeof userId !== 'string' ||
                typeof idMediaSosial !== 'number' ||
                typeof namaMediaSosial !== 'string' ||
                typeof username !== 'string' ||
                typeof credential !== 'object') {
                    res.status(400).send({
                        "error": "Bad input format",
                    });
                    return;
            }

            // mengecek apakah media sosial tersebut ada
            var mediaSosial = await prisma.mediaSosial.findUnique({ 
                where: {
                    id: idMediaSosial,
                },
            });

            if (!mediaSosial) {
                res.status(404).send({
                    "error": "MediaSosial not found",
                });
                return;
            }

            // validasi apakah user ada dalam tim dan memiliki role PR atau ADMINISTRATOR
            var idTim = mediaSosial.idTim;
            var roleDalamTim = await prisma.roleDalamTim.findUnique({ 
                where: {
                    userId_idTim: {userId, idTim},
                },
            });

            if (!roleDalamTim || !(roleDalamTim.role == Role.PR || roleDalamTim.role == Role.ADMINISTRATOR)) {
                res.status(403).send({
                    "error": "No access",
                });
                return;
            }

            // update
            mediaSosial = await prisma.mediaSosial.update({
                where: {
                    id: idMediaSosial,
                },
                data: {
                    namaMediaSosial: namaMediaSosial,
                    username: username,
                    credential: credential,
                },
            });

            res.status(200).send({
                mediaSosial,
            });
        } catch (err) {
            console.log(err);
        }
    });

    app.delete('/mediasosial', async function(req, res) {
        try {
            var { userId, idMediaSosial } = req.body;
            
            if (typeof userId === 'undefined' ||
                typeof idMediaSosial === 'undefined') {
                    res.status(400).send({
                        "error": "All input is required",
                    });
                    return;
            }
            idMediaSosial = Number(idMediaSosial);
            if (typeof userId !== 'string' ||
                typeof idMediaSosial !== 'number') {
                    res.status(400).send({
                        "error": "Bad input format",
                    });
                    return;
            }

            // mengecek apakah media sosial tersebut ada
            var mediaSosial = await prisma.mediaSosial.findUnique({ 
                where: {
                    id: idMediaSosial,
                },
            });

            if (!mediaSosial) {
                res.status(404).send({
                    "error": "MediaSosial not found",
                });
                return;
            }

            // validasi apakah user ada dalam tim dan memiliki role PR atau ADMINISTRATOR
            var idTim = mediaSosial.idTim;
            var roleDalamTim = await prisma.roleDalamTim.findUnique({ 
                where: {
                    userId_idTim: {userId, idTim},
                },
            });

            if (!roleDalamTim || !(roleDalamTim.role == Role.PR || roleDalamTim.role == Role.ADMINISTRATOR)) {
                res.status(403).send({
                    "error": "No access",
                });
                return;
            }

            // delete
            mediaSosial = await prisma.mediaSosial.delete({
                where: {
                    id: idMediaSosial,
                },
            });

            res.status(204).send();
        } catch (err) {
            console.log(err);
        }
    });
}