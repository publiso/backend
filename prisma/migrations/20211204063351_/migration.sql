-- CreateEnum
CREATE TYPE "Role" AS ENUM ('ADMINISTRATOR', 'GA', 'PR', 'DSG');

-- CreateEnum
CREATE TYPE "Status" AS ENUM ('dalam_review_pr', 'dalam_review_dsg', 'menunggu_desain', 'dalam_verifikasi', 'dijadwalkan', 'ditolak', 'selesai');

-- CreateTable
CREATE TABLE "Tim" (
    "id" SERIAL NOT NULL,
    "namaTim" TEXT NOT NULL,

    CONSTRAINT "Tim_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "RoleDalamTim" (
    "userId" TEXT NOT NULL,
    "role" "Role" NOT NULL,
    "idTim" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "MediaSosial" (
    "id" SERIAL NOT NULL,
    "idTim" INTEGER NOT NULL,
    "namaMediaSosial" TEXT NOT NULL,
    "credential" JSONB NOT NULL,

    CONSTRAINT "MediaSosial_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "PermintaanPublikasi" (
    "id" SERIAL NOT NULL,
    "userId" TEXT NOT NULL,
    "idTim" INTEGER NOT NULL,
    "status" "Status" NOT NULL,
    "tanggalWaktuPublikasi" TIMESTAMP(3) NOT NULL,
    "judulPublikasi" TEXT NOT NULL,
    "kontenPublikasi" TEXT NOT NULL,
    "deskripsiDesainPublikasi" TEXT NOT NULL,
    "buktiMendesak" TEXT,

    CONSTRAINT "PermintaanPublikasi_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "MediaSosialUntukPermintaanPublikasi" (
    "id" SERIAL NOT NULL,
    "idPermintaanPublikasi" INTEGER NOT NULL,
    "idMediaSosial" INTEGER NOT NULL,

    CONSTRAINT "MediaSosialUntukPermintaanPublikasi_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Komentar" (
    "id" SERIAL NOT NULL,
    "teksKomentar" TEXT NOT NULL,
    "userId" TEXT NOT NULL,
    "idPermintaanPublikasi" INTEGER NOT NULL,

    CONSTRAINT "Komentar_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "LinkInvitasi" (
    "slugInvitasi" TEXT NOT NULL,
    "role" "Role" NOT NULL,
    "idTim" INTEGER NOT NULL,
    "expiry" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "LinkInvitasi_pkey" PRIMARY KEY ("slugInvitasi")
);

-- CreateIndex
CREATE UNIQUE INDEX "RoleDalamTim_userId_idTim_key" ON "RoleDalamTim"("userId", "idTim");

-- AddForeignKey
ALTER TABLE "RoleDalamTim" ADD CONSTRAINT "RoleDalamTim_idTim_fkey" FOREIGN KEY ("idTim") REFERENCES "Tim"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "MediaSosial" ADD CONSTRAINT "MediaSosial_idTim_fkey" FOREIGN KEY ("idTim") REFERENCES "Tim"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PermintaanPublikasi" ADD CONSTRAINT "PermintaanPublikasi_idTim_fkey" FOREIGN KEY ("idTim") REFERENCES "Tim"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "MediaSosialUntukPermintaanPublikasi" ADD CONSTRAINT "MediaSosialUntukPermintaanPublikasi_idMediaSosial_fkey" FOREIGN KEY ("idMediaSosial") REFERENCES "MediaSosial"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "MediaSosialUntukPermintaanPublikasi" ADD CONSTRAINT "MediaSosialUntukPermintaanPublikasi_idPermintaanPublikasi_fkey" FOREIGN KEY ("idPermintaanPublikasi") REFERENCES "PermintaanPublikasi"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Komentar" ADD CONSTRAINT "Komentar_idPermintaanPublikasi_fkey" FOREIGN KEY ("idPermintaanPublikasi") REFERENCES "PermintaanPublikasi"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "LinkInvitasi" ADD CONSTRAINT "LinkInvitasi_idTim_fkey" FOREIGN KEY ("idTim") REFERENCES "Tim"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
