/*
  Warnings:

  - You are about to drop the column `deskripsiDesainPublikasi` on the `PermintaanPublikasi` table. All the data in the column will be lost.
  - You are about to drop the column `kontenPublikasi` on the `PermintaanPublikasi` table. All the data in the column will be lost.
  - Added the required column `username` to the `MediaSosial` table without a default value. This is not possible if the table is not empty.
  - Added the required column `bahanKonten` to the `PermintaanPublikasi` table without a default value. This is not possible if the table is not empty.
  - Added the required column `catatan` to the `PermintaanPublikasi` table without a default value. This is not possible if the table is not empty.
  - Added the required column `idLinePemesan` to the `PermintaanPublikasi` table without a default value. This is not possible if the table is not empty.
  - Added the required column `isMendesak` to the `PermintaanPublikasi` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "MediaSosial" ADD COLUMN     "username" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "PermintaanPublikasi" DROP COLUMN "deskripsiDesainPublikasi",
DROP COLUMN "kontenPublikasi",
ADD COLUMN     "bahanKonten" TEXT NOT NULL,
ADD COLUMN     "catatan" TEXT NOT NULL,
ADD COLUMN     "idLinePemesan" TEXT NOT NULL,
ADD COLUMN     "isMendesak" BOOLEAN NOT NULL,
ADD COLUMN     "tanggalWaktuPemesanan" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP;
