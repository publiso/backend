# backend

URL: https://publisoprojectapi.herokuapp.com/

DATABASE: relasional

## Documentation
Lihat [folder docs](./docs).

## Notes
- Untuk menggunakan database di komputer lokal, buat database baru tambahkan file `.env` di project root folder yang berisi variabel `DATABASE_URL`.
    Contoh (dengan database `publiso`):
    ```
    DATABASE_URL="postgresql://postgres:postgres@localhost:5432/publiso"
    ```
- *Migrate database* (locally):
    ```
    npx prisma migrate dev
    ```
  Hasil migrasi harus di-commit, karena command untuk deploy tidak mengecek perubahan pada file `prisma.schema` ([referensi](https://www.prisma.io/docs/concepts/components/prisma-migrate)).
- *Environment variables* yang perlu diatur:
    - `TOKEN_KEY`, berisi sebuah *random* string untuk JWT.
- *How to run*:
    ```
    npm run start
    ```
  Jika butuh *hot reload*:
    ```
    npm run dev
    ```

## References
- [Mengenai Prisma](https://www.prisma.io/docs/getting-started/setup-prisma/start-from-scratch/relational-databases-node-postgres)
- [Mengenai Express](https://expressjs.com/)