const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient();

async function isUser(userId) {
    const result = await prisma.roleDalamTim.findFirst({
        where: {
            id: userId
        }
    });
    return result !== null && result !== undefined;
}

module.exports = { prisma, isUser };