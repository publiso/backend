# Mengubah Media Sosial Milik Tim
Mengubah media sosial milik tim. Aksi ini hanya diperbolehkan untuk PR atau ADMINISTRATOR.

**URL** : `/mediasosial`

**Method** : `PUT`

**Auth required** : YES

**Data Constraints**

```json
{
    "userId": "[userId berupa string]",
    "idMediaSosial": "[id dari media sosial, berupa angka]",
    "namaMediaSosial": "[nama media sosial (misal: instagram), berupa string]",
    "username": "[username dari media sosial, berupa string]",
    "credential": "[credential yang ingin disimpan (contoh: api key), berupa json]"
}
```

**Data Example**
```json
{
    "userId": "userAdmin",
    "idMediaSosial": 1,
    "namaMediaSosial": "twitter",
    "username": "usernameTwitterBaru",
    "credential": {
        "api_key": "abcdefBaru",
        "api_key_1": "abcdefBaru"
    }
}
```
## Success Response
**Code** : `200 OK`

**Content example**

```json
{
    "mediaSosial": {
        "id": 1,
        "idTim": 1,
        "namaMediaSosial": "twitter",
        "username": "usernameTwitterBaru",
        "credential": {
            "api_key": "abcdefBaru",
            "api_key_1": "abcdefBaru"
        }
    }
}
```

## Error Response

-   **Condition** : Jika terdapat setidaknya salah satu parameter wajib yang kosong.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "All input is required"
    }
    ```

-   **Condition** : Jika terdapat setidaknya salah satu parameter yang tipe datanya tidak sesuai.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "Bad input format"
    }
    ```

-   **Condition** : Jika media sosial tidak ditemukan dalam database.

    **Code** : `404 NOT FOUND`

    **Content** :
    ```json
    {
        "error": "MediaSosial not found"
    }
    ```
-   **Condition** : Jika pengguna tidak memiliki *role* dalam tim, atau pengguna tidak memiliki *role* PR atau ADMINISTRATOR dalam tim tersebut.

    **Code** : `403 FORBIDDEN`

    **Content** :
    ```json
    {
        "error": "No access"
    }
    ```