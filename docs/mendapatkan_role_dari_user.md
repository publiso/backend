# Mendapatkan Role dari User
*Endpoint* untuk mendapatkan *role* dari *user*. *Role* yang dikembalikan adalah salah satu antara `ADMINISTRATOR`, `DSG`, `PR`, dan `GA`.

**URL** : `/role_dalam_tim`

**Method** : `GET`

**Auth required** : YES

**Data Constraints**

```json
{
    "userId": "[userId berupa string]",
    "idTim": "[id dari tim yang ingin diketahui, berupa angka, opsional]",
}
```

**Data Example**
-   ```json
    {
        "userId": "userAdmin"
    }
    ```
-   ```json
    {
        "userId": "userAdmin",
        "idTim": 1
    }
    ```

## Success Response
**Code** : `200 OK`

**Content example**

-   Jika `idTim` tidak diberikan.
    ```json
    {
        "roleDalamTim": [
            {
                "role": "ADMINISTRATOR",
                "idTim": 1
            }
        ]
    }
    ```
-   Jika `idTim` diberikan.
    ```json
    {
        "roleDalamTim": [
            {
                "role": "ADMINISTRATOR"
            }
        ]
    }
    ```
-   Jika tidak ada *role* sama sekali.
    ```json
    {
        "roleDalamTim": []
    }
    ```

## Error Response

-   **Condition** : Jika tidak terdapat `userId` pada *request*.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "userId is required"
    }
    ```

-   **Condition** : Jika `userId` bukan sebuah string.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "Bad input format"
    }
    ```