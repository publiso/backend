# Membuat Permintaan Publikasi
Membuat permintaan publikasi. Aksi ini dapat dilakukan oleh ADMINISTRATOR atau GA.

**URL** : `/buat_permintaan_publikasi`

**Method** : `POST`

**Auth required** : YES

**Data Constraints**

```json
{
    "userId": "[userId berupa string]",
    "idTim": "[idTim berupa angka]",
    "judulPublikasi": "[judul]",
    "idLinePemesan": "[idLine]",
    "tanggalWaktuPublikasi": "[tanggal dan waktu mengikuti format YYYY-MM-DDThh:mm:ss.sssZ]",
    "bahanKonten": "[bahan konten, berupa string]",
    "isMendesak": "[boolean yang menyatakan keadaan mendesak (true) atau tidak (false)]",
    "buktiMendesak": "[bukti bahwa permintaan ini mendesak, berupa string, tidak perlu diisi jika isMendesak bernilai false, wajib diisi ketika isMendesak bernilai true]",
    "mediaSosial": "[list berisi objek yang menunjukkan id media sosial yang dipilih]",
    "catatan": "[catatan dari permintaan publikasi]"
}
```
- Referensi format tanggal dan waktu dapat dilihat [di sini](https://help.prisma-capacity.eu/support/solutions/articles/36000113825-what-format-do-timestamps-have-in-the-prisma-api-).

**Data Example**
```json
{
    "userId": "userGA",
    "idTim": 1,
    "judulPublikasi": "judul",
    "idLinePemesan": "idLine",
    "tanggalWaktuPublikasi": "2022-01-01T00:00:00Z",
    "bahanKonten": "bahanKonten",
    "isMendesak": true,
    "buktiMendesak": "ini mendesak",
    "mediaSosial": [
        {
            "id": 1
        }
    ],
    "catatan": ""
}
```
## Success Response
**Code** : `201 CREATED`

**Content example**

```json
{
    "permintaanPublikasi": {
        "id": 1,
        "userId": "userGA",
        "idTim": 1,
        "status": "dalam_review_pr",
        "tanggalWaktuPublikasi": "2022-01-01T00:00:00.000Z",
        "judulPublikasi": "judul",
        "buktiMendesak": "ini mendesak",
        "bahanKonten": "bahanKonten",
        "hasilDesain": null,
        "catatan": "",
        "idLinePemesan": "idLine",
        "isMendesak": true,
        "tanggalWaktuPemesanan": "2021-12-06T03:00:52.796Z",
        "MediaSosialUntukPermintaanPublikasi": [
            {
                "MediaSosial": {
                    "namaMediaSosial": "instagram",
                    "username": "ourInstagram"
                }
            }
        ]
    }
}
```

## Error Response

-   **Condition** : Jika terdapat setidaknya salah satu parameter wajib yang kosong.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "All input is required"
    }
    ```

-   **Condition** : Jika terdapat setidaknya salah satu parameter yang tipe datanya tidak sesuai.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "Bad input format"
    }
    ```
-   **Condition** : Jika tim dengan `idTim` tidak ditemukan dalam database.

    **Code** : `404 NOT FOUND`

    **Content** :
    ```json
    {
        "error": "Tim not found"
    }
    ```

-   **Condition** : Jika pengguna tidak memiliki *role* dalam tim, atau pengguna tidak memiliki *role* GA atau ADMINISTRATOR dalam tim tersebut.

    **Code** : `403 FORBIDDEN`

    **Content** :
    ```json
    {
        "error": "No access"
    }
    ```


-   **Condition**: Jika tim ditemukan dan pengguna memiliki akses, namun media sosial yang dicari tidak ditemukan sebagai milik tim.

    **Code** : `404 NOT FOUND`

    **Content** :
    ```json
    {
        "error": "MediaSosial not found"
    }
    ```