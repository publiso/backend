# Mendapatkan Data dari Permintaan Publikasi Berdasarkan ID
Mendapatkan data permintaan publikasi berdasarkan ID. Jika permintaan ini dilakukan pengguna dengan *role* GA, maka yang dikembalikan hanya permintaan publikasi miliknya. Jika pengguna dengan *role* lain yang melakukannya, maka seluruh permintaan dengan status tertentu pada tim tersebut dikembalikan.

**URL** : `/get_permintaan_publikasi/mendesak`

**Method** : `GET`

**Auth required** : YES

**Data Constraints**

```json
{
    "userId": "[userId berupa string]",
    "idPermintaanPublikasi": "[id permintaan publikasi berupa angka]"
}
```

**Data Example**
```json
{
    "userId": "userGA",
    "idPermintaanPublikasi": 1
}
```
## Success Response
**Code** : `200 OK`

**Content example**

```json
{
    "permintaanPublikasi": {
        "id": 1,
        "userId": "userGA",
        "idTim": 1,
        "status": "dalam_review_pr",
        "tanggalWaktuPublikasi": "2022-01-01T00:00:00.000Z",
        "judulPublikasi": "judul",
        "buktiMendesak": "ini mendesak",
        "bahanKonten": "bahanKonten",
        "hasilDesain": null,
        "catatan": "",
        "idLinePemesan": "idLine",
        "isMendesak": true,
        "tanggalWaktuPemesanan": "2021-12-06T03:00:52.796Z",
        "MediaSosialUntukPermintaanPublikasi": [
            {
                "MediaSosial": {
                    "namaMediaSosial": "instagram",
                    "username": "ourInstagram"
                }
            }
        ]
    }
}
```

## Error Response

-   **Condition** : Jika terdapat setidaknya salah satu parameter wajib yang kosong.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "All input is required"
    }
    ```

-   **Condition** : Jika terdapat setidaknya salah satu parameter yang tipe datanya tidak sesuai.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "Bad input format"
    }
    ```
-   **Condition** : Jika permintaan publikasi tidak ditemukan.

    **Code** : `404 NOT FOUND`

    **Content** :
    ```json
    {
        "error": "PermintaanPublikasi not found"
    }
    ```
-   **Condition** : Jika pengguna tidak memiliki *role* dalam tim yang memiliki permintaan publikasi tersebut, atau pengguna memiliki *role* GA dan permintaan publikasi tersebut bukanlah miliknya.
    **Code** : `403 FORBIDDEN`

    **Content** :
    ```json
    {
        "error": "No access"
    }
    ```