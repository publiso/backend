# Mendapatkan Data Tim
*Endpoint* untuk mendapatkan data tim, dapat dilakukan oleh pengguna yang memiliki *role* dalam tim tersebut.

**URL** : `/tim`

**Method** : `GET`

**Auth required** : YES

**Data Constraints**

```json
{
    "userId": "[userId berupa string]",
    "idTim": "[id dari tim yang ingin diketahui, berupa angka]",
}
```

**Data Example**
```json
{
    "userId": "userAdmin",
    "idTim": 1
}
```
## Success Response
**Code** : `200 OK`

**Content example**

```json
{
    "tim": {
        "id": 1,
        "namaTim": "tim1"
    }
}
```

## Error Response

-   **Condition** : Jika terdapat setidaknya salah satu parameter wajib yang kosong.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "All input is required"
    }
    ```


-   **Condition** : Jika terdapat setidaknya salah satu parameter yang tipe datanya tidak sesuai.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "Bad input format"
    }
    ```

-   **Condition** : Jika tim dengan `idTim` tidak ditemukan dalam database.

    **Code** : `404 NOT FOUND`

    **Content** :
    ```json
    {
        "error": "Tim not found"
    }
    ```
-   **Condition** : Jika pengguna tidak memiliki *role* dalam tim.

    **Code** : `403 FORBIDDEN`

    **Content** :
    ```json
    {
        "error": "No access"
    }
    ```