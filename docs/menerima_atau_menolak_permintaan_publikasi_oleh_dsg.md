# Menerima atau Menolak Permintaan Publikasi Oleh DSG
Fitur ini akan mengubah status dari permintaan publikasi yang berstatus `dalam_review_dsg` menjadi `menunggu_desain` jika diterima, atau `ditolak` jika ditolak.

**URL** : `/review_dsg`

**Method** : `POST`

**Auth required** : YES

**Data Constraints**

```json
{
    "userId": "[userId berupa string]",
    "idPermintaanPublikasi": "[id permintaan publikasi yang ingin diubah statusnya, berupa angka]",
    "terima": "[boolean yang menyatakan permintaan publikasi diterima (true) atau ditolak (false)]"
}
```

**Data Example**

```json
{
    "userId": "userAdmin",
    "idPermintaanPublikasi": 1,
    "terima": true
}
```

## Success Response
**Code** : `204 NO CONTENT`

**Content example**
> Intentionally left empty

## Error Response

-   **Condition** : Jika terdapat setidaknya salah satu parameter wajib yang kosong.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```
    {
        "error": "All input is required"
    }
    ```

-   **Condition** : Jika terdapat setidaknya salah satu parameter yang tipe datanya tidak sesuai.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```
    {
        "error": "Bad input format"
    }
    ```

-   **Condition** : Jika `idPermintaanPublikasi` tidak ditemukan, atau statusnya bukan `dalam_review_dsg`.

    **Code** : `404 NOT FOUND`

    **Content** :
    ```json
    {
        "error": "PermintaanPublikasi not found"
    }
    ```

-   **Condition** : Jika publikasinya ada, statusnya `dalam_review_dsg`, tetapi `user_id` tidak berhak menerima / menolak permintaan publikasi (bukan dalam tim yang sama dengan permintaan publikasi, atau *role*-nya bukan ADMINISTRATOR atau DSG).

    **Code** : `403 FORBIDDEN`

    **Content** :
    ```json
    {
        "error": "No access"
    }
    ```