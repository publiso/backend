# Mendapatkan Arsip Publikasi
Mendapatkan semua permintaan publikasi pada tim. Aksi ini dapat dilakukan pengguna dengan *role* ADMINISTRATOR pada tim.

**URL** : `/arsip`

**Method** : `GET`

**Auth required** : YES

**Data Constraints**

```json
{
    "userId": "[userId berupa string]",
    "idTim": "[idTim berupa angka]"
}
```

**Data Example**
```json
{
    "userId": "userGA",
    "idTim": 1
}
```
## Success Response
**Code** : `200 OK`

**Content example**

```json
{
    "permintaanPublikasi": [
        {
            "id": 1,
            "userId": "userGA",
            "idTim": 1,
            "status": "dalam_review_pr",
            "tanggalWaktuPublikasi": "2022-01-01T00:00:00.000Z",
            "judulPublikasi": "judul",
            "buktiMendesak": "ini mendesak",
            "bahanKonten": "bahanKonten",
            "hasilDesain": null,
            "catatan": "",
            "idLinePemesan": "idLine",
            "isMendesak": true,
            "tanggalWaktuPemesanan": "2021-12-06T03:00:52.796Z",
            "MediaSosialUntukPermintaanPublikasi": [
                {
                    "MediaSosial": {
                        "namaMediaSosial": "instagram",
                        "username": "ourInstagram"
                    }
                }
            ]
        }
    ]
}
```

## Error Response

-   **Condition** : Jika terdapat setidaknya salah satu parameter wajib yang kosong.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "All input is required"
    }
    ```

-   **Condition** : Jika terdapat setidaknya salah satu parameter yang tipe datanya tidak sesuai.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "Bad input format"
    }
    ```
-   **Condition** : Jika tim dengan `idTim` tidak ditemukan dalam database.

    **Code** : `404 NOT FOUND`

    **Content** :
    ```json
    {
        "error": "Tim not found"
    }
    ```
-   **Condition** : Jika pengguna tidak memiliki *role* ADMINISTRATOR dalam tim tersebut.
    **Code** : `403 FORBIDDEN`

    **Content** :
    ```json
    {
        "error": "No access"
    }
    ```