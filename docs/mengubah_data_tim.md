# Mengubah Data Tim
Mengubah data tim. Aksi ini dapat dilakukan ADMINISTRATOR dalam tim.

**URL** : `/tim`

**Method** : `PUT`

**Auth required** : YES

**Data Constraints**

```json
{
    "userId": "[userId berupa string]",
    "idTim": "[id dari tim, berupa angka]",
    "namaTim": "[nama tim, berupa string]",
}
```

**Data Example**
```json
{
    "userId": "userAdmin",
    "idTim": 1,
    "namaTim": "namaTimBaru"
}
```
## Success Response
**Code** : `200 OK`

**Content example**

```json
{
    "tim": {
        "id": 1,
        "namaTim": "namaTimBaru"
    }
}
```

## Error Response

-   **Condition** : Jika terdapat setidaknya salah satu parameter wajib yang kosong.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "All input is required"
    }
    ```

-   **Condition** : Jika terdapat setidaknya salah satu parameter yang tipe datanya tidak sesuai.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "Bad input format"
    }
    ```

-   **Condition** : Jika tim dengan `idTim` tidak ditemukan dalam database.

    **Code** : `404 NOT FOUND`

    **Content** :
    ```json
    {
        "error": "Tim not found"
    }
    ```
-   **Condition** : Jika pengguna tidak memiliki *role* dalam tim, atau pengguna tidak memiliki *role* ADMINISTRATOR dalam tim tersebut.

    **Code** : `403 FORBIDDEN`

    **Content** :
    ```json
    {
        "error": "No access"
    }
    ```