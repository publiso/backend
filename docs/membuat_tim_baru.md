# Membuat Tim Baru
Membuat tim baru. Pengguna otomatis menjadi ADMINISTRATOR dari tim tersebut.

**URL** : `/tim`

**Method** : `POST`

**Auth required** : YES

**Data Constraints**

```json
{
    "userId": "[userId berupa string]",
    "namaTim": "[nama tim, berupa string]"
}
```

**Data Example**
```json
{
    "userId": "userAdmin",
    "namaTim": "iniNamaTim"
}
```
## Success Response
**Code** : `201 CREATED`

**Content example**

```json
{
    "tim": {
        "id": 3,
        "namaTim": "iniNamaTim"
    }
}
```

## Error Response

-   **Condition** : Jika terdapat setidaknya salah satu parameter wajib yang kosong.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "All input is required"
    }
    ```

-   **Condition** : Jika terdapat setidaknya salah satu parameter yang tipe datanya tidak sesuai.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "Bad input format"
    }
    ```