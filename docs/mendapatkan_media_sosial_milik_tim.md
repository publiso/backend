# Mendapatkan Media Sosial Milik Tim
*Endpoint* untuk mendapatkan semua media sosial yang dimiliki tim. Dapat dilakukan semua anggota tim dengan respon yang berbeda untuk *role* yang berbeda.

**URL** : `/mediasosial`

**Method** : `GET`

**Auth required** : YES

**Data Constraints**

```json
{
    "userId": "[userId berupa string]",
    "idTim": "[id dari tim yang ingin diketahui, berupa angka]",
}
```

**Data Example**
```json
{
    "userId": "userAdmin",
    "idTim": 1
}
```
## Success Response
**Code** : `200 OK`

**Content example**

-   Jika yang melakukan *request* memiliki *role* PR atau ADMINISTRATOR.
    ```json
    {
        "mediaSosial": [
            {
                "id": 1,
                "idTim": 1,
                "namaMediaSosial": "instagram",
                "username": "ourInstagram",
                "credential": {
                    "api_key": "api_key_instagram_1"
                }
            }
        ]
    }
    ```
-   Jika yang melakukan *request* memiliki *role* GA atau DSG.
    ```json
    {
        "mediaSosial": [
            {
                "namaMediaSosial": "instagram",
                "username": "ourInstagram"
            }
        ]
    }
    ```

## Error Response

-   **Condition** : Jika terdapat setidaknya salah satu parameter wajib yang kosong.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "All input is required"
    }
    ```


-   **Condition** : Jika terdapat setidaknya salah satu parameter yang tipe datanya tidak sesuai.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "Bad input format"
    }
    ```

-   **Condition** : Jika tim dengan `idTim` tidak ditemukan dalam database.

    **Code** : `404 NOT FOUND`

    **Content** :
    ```json
    {
        "error": "Tim not found"
    }
    ```
-   **Condition** : Jika pengguna tidak memiliki *role* dalam tim.

    **Code** : `403 FORBIDDEN`

    **Content** :
    ```json
    {
        "error": "No access"
    }
    ```