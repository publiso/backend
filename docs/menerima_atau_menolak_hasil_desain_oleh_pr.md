# Menerima atau Menolak Hasil Desain Oleh PR
Fitur ini akan mengubah status dari permintaan publikasi yang berstatus `menunggu_desain` menjadi `dijadwalkan` jika diterima, atau `menunggu_desain` jika ditolak.

**URL** : `/verifikasi_pr`

**Method** : `POST`

**Auth required** : YES

**Data Constraints**

```json
{
    "userId": "[userId berupa string]",
    "idPermintaanPublikasi": "[id permintaan publikasi yang ingin diverifikasi desainnya berupa angka]",
    "terima": "[boolean yang menyatakan desain diterima (true) atau ditolak (false)]"
}
```

**Data Example**

```json
{
    "userId": "userAdmin",
    "idPermintaanPublikasi": 1,
    "terima": true
}
```

## Success Response
**Code** : `204 NO CONTENT`

**Content example**
> Intentionally left empty

## Error Response

-   **Condition** : Jika terdapat setidaknya salah satu parameter wajib yang kosong.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```
    {
        "error": "All input is required"
    }
    ```

-   **Condition** : Jika terdapat setidaknya salah satu parameter yang tipe datanya tidak sesuai.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```
    {
        "error": "Bad input format"
    }
    ```

-   **Condition** : Jika `idPermintaanPublikasi` tidak ditemukan, atau statusnya bukan `dalam_verifikasi`.

    **Code** : `404 NOT FOUND`

    **Content** :
    ```json
    {
        "error": "PermintaanPublikasi not found"
    }
    ```

-   **Condition** : Jika publikasinya ada, statusnya `dalam_verifikasi`, tetapi `user_id` tidak berhak menerima / menolak hasil desain (bukan dalam tim yang sama dengan permintaan publikasi, atau *role*-nya bukan ADMINISTRATOR atau PR).

    **Code** : `403 FORBIDDEN`

    **Content** :
    ```json
    {
        "error": "No access"
    }
    ```