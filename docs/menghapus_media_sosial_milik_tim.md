# Menghapus Media Sosial Milik Tim
Menghapus media sosial milik tim. Aksi ini hanya diperbolehkan untuk PR atau ADMINISTRATOR.

**URL** : `/mediasosial`

**Method** : `DELETE`

**Auth required** : YES

**Data Constraints**

```json
{
    "userId": "[userId berupa string]",
    "idMediaSosial": "[id dari media sosial, berupa angka]",
}
```

**Data Example**
```json
{
    "userId": "userAdmin",
    "idMediaSosial": 1
}
```
## Success Response
**Code** : `204 NO CONTENT`

**Content example**

> Intentionally left empty

## Error Response

-   **Condition** : Jika terdapat setidaknya salah satu parameter wajib yang kosong.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "All input is required"
    }
    ```

-   **Condition** : Jika terdapat setidaknya salah satu parameter yang tipe datanya tidak sesuai.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "Bad input format"
    }
    ```

-   **Condition** : Jika media sosial tidak ditemukan dalam database.

    **Code** : `404 NOT FOUND`

    **Content** :
    ```json
    {
        "error": "MediaSosial not found"
    }
    ```
-   **Condition** : Jika pengguna tidak memiliki *role* dalam tim, atau pengguna tidak memiliki *role* PR atau ADMINISTRATOR dalam tim tersebut.

    **Code** : `403 FORBIDDEN`

    **Content** :
    ```json
    {
        "error": "No access"
    }
    ```