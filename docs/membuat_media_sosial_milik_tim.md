# Membuat Media Sosial Milik Tim
Membuat media sosial milik tim. Aksi ini hanya diperbolehkan untuk PR atau ADMINISTRATOR.

**URL** : `/mediasosial`

**Method** : `POST`

**Auth required** : YES

**Data Constraints**

```json
{
    "userId": "[userId berupa string]",
    "idTim": "[id dari tim, berupa angka]",
    "namaMediaSosial": "[nama media sosial (misal: instagram), berupa string]",
    "username": "[username dari media sosial, berupa string]",
    "credential": "[credential yang ingin disimpan (contoh: api key), berupa json]"
}
```

**Data Example**
```json
{
    "userId": "userAdmin",
    "idTim": 1,
    "namaMediaSosial": "twitter",
    "username": "usernameTwitter",
    "credential": {
        "api_key": "abcdef",
        "api_key_1": "abcdef"
    }
}
```
## Success Response
**Code** : `201 CREATED`

**Content example**

```json
{
    "mediaSosial": {
        "id": 1,
        "idTim": 1,
        "namaMediaSosial": "twitter",
        "username": "usernameTwitter",
        "credential": {
            "api_key": "abcdef",
            "api_key_1": "abcdef"
        }
    }
}
```

## Error Response

-   **Condition** : Jika terdapat setidaknya salah satu parameter wajib yang kosong.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "All input is required"
    }
    ```

-   **Condition** : Jika terdapat setidaknya salah satu parameter yang tipe datanya tidak sesuai.

    **Code** : `400 BAD REQUEST`

    **Content** :
    ```json
    {
        "error": "Bad input format"
    }
    ```

-   **Condition** : Jika tim dengan `idTim` tidak ditemukan dalam database.

    **Code** : `404 NOT FOUND`

    **Content** :
    ```json
    {
        "error": "Tim not found"
    }
    ```
-   **Condition** : Jika pengguna tidak memiliki *role* dalam tim, atau pengguna tidak memiliki *role* PR atau ADMINISTRATOR dalam tim tersebut.

    **Code** : `403 FORBIDDEN`

    **Content** :
    ```json
    {
        "error": "No access"
    }
    ```