const express = require('express');
const app = express();

const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const hostname = '0.0.0.0';
const port = process.env.PORT || 3000;

app.use(express.json());
require('./routes')(app, prisma);

app.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});
